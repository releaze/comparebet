<?php
/*
Template Name: Home Page Template
Template Post Type: page
*/

get_header();
$supportedBookmakers  = si_article_builder_get_sb_bookmakers_list();
$supportedTournaments = si_article_builder_get_sb_leagues_list();
?>
    <div class="container content">
        <div class="row">
            <div class="col-lg-12 col-xl-9 center-content home-page">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <si-lb-widget
                            widget-id="TopMatchesOddsCompare"
                            nopadding="true"
                            transparent="true"
                            width="100%"
                            data-allow-collapse="true"
                            supported-tournaments="<?php echo implode(",", $supportedTournaments); ?>"
                            providers="<?php echo implode(",", $supportedBookmakers); ?>"
                            ></si-lb-widget>
                        <si-lb-widget
                            widget-id="FixturesOddsCompare"
                            width="100%" nopadding="true"
                            transparent="true"
                            data-nopadding="true"
                            initial-collapse="false"
                            allow-collapse="false"
                            supported-tournaments="<?php echo implode(",", $supportedTournaments); ?>"
                            providers="<?php echo implode(",", $supportedBookmakers); ?>"
                        ></si-lb-widget>

                        <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/home', 'page' );

                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                        ?>

                    </main>
                </div>
            </div>
            <div class="col-lg-8 offset-lg-4 col-xl-3 right-sidebar">
                 <aside class="widget-area">
                     <?php dynamic_sidebar( 'right-sidebar' ); ?>
                 </aside>
            </div>
        </div>
    </div>
<?php
get_footer();
