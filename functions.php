<?php
/**
 * comparebet functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package comparebet
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.1' );
}

if ( ! function_exists( 'comparebet_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function comparebet_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on comparebet, use a find and replace
		 * to change 'comparebet' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'comparebet', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'comparebet' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'comparebet_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'comparebet_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function comparebet_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'comparebet_content_width', 640 );
}
add_action( 'after_setup_theme', 'comparebet_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function comparebet_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'comparebet' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'comparebet' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar( 
		array(
			'name' => 'Seconday Header Menu',
			'id' => 'secondary_header_menu',
			'before_widget' => '<div>',
			'after_widget' => '</div>',
			'before_title' => '<h2 class="rounded">',
			'after_title' => '</h2>',
	  ));
}
add_action( 'widgets_init', 'comparebet_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function comparebet_scripts() {
	wp_enqueue_style( 'comparebet-style', get_stylesheet_uri(), array(), _S_VERSION );
	wp_style_add_data( 'comparebet-style', 'rtl', 'replace' );

	wp_enqueue_script( 'comparebet-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'comparebet-widgets', get_template_directory_uri() . '/js/widgets.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	$evergreen_posts_config = array();

	$evergreen_posts = get_posts(
		array(
			'posts_per_page' => -1,
// 			'tag'            => 'ab-evergreen-teams',
			'post_status'    => array( 'publish' ),
			'post_type'      => 'post',
			'orderby'        => 'date',
			'order'          => 'DESC',
			'meta_query' => array(
				array( 'key' => 'team_1', 'compare' => 'EXISTS' ),
				array( 'key' => 'team_2', 'compare' => 'EXISTS' )
			)
		)
	);

	foreach( $evergreen_posts as $post ) {
		setup_postdata($post);
		$post_id = $post->ID;

		$result = array(
			'postId'  => $post_id,
			'teamId1' => get_post_meta($post_id, 'team_1', true),
			'teamId2' => get_post_meta($post_id, 'team_2', true),
			'url'     => get_permalink($post_id)
		);

		array_push($evergreen_posts_config, $result);
	}

	wp_reset_postdata();

	$config  = array( 'evergreenPosts' => $evergreen_posts_config);

	wp_localize_script( 'comparebet-navigation', '__COMPAREBET_CONFIG', $config);
	wp_localize_script( 'comparebet-widgets', '__COMPAREBET_CONFIG', $config);
}
add_action( 'wp_enqueue_scripts', 'comparebet_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Custom Widgets.
 */
require get_template_directory() . '/widgets/si-recent-post-widget.php';
require get_template_directory() . '/widgets/dt9-leagues-menu-widget.php';
require get_template_directory() . '/widgets/widgets.php';
require get_stylesheet_directory() . '/helpers.php';


//======================================================================================================================
add_action( 'wp_enqueue_scripts', 'comparebet_style_theme' );
add_action( 'widgets_init', 'comparebet_register_sidebar' );
add_action( 'after_setup_theme', 'comparebet_recent_posts_image_size' );
add_action( 'after_setup_theme', 'comparebet_fullwidth_latest_image_size' );
add_action( 'after_setup_theme', 'comparabet_latest_image_size' );
add_action( 'after_setup_theme', 'comparabet_latest_large_image_size' );




add_shortcode( 'latest_posts', 'comparebet_latest_posts_shortcode_function' );
add_action( 'init', 'comparebet_footer_menus' );
add_shortcode( 'fullwidth_latest_post', 'comparebet_fullwidth_latest_post_shortcode_function' );

// Adds styles
function comparebet_style_theme(){
    wp_enqueue_style( 'bootstrap_grid', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.3/css/bootstrap-grid.min.css');
    wp_enqueue_style( 'roboto_font', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap' );
    wp_enqueue_style( 'montserrat_font', 'https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&display=swap' );
    wp_enqueue_style( 'style', get_stylesheet_uri() );
}

function comparebet_register_sidebar(){
	register_sidebar( array(
		'name' => "Right Sidebar",
		'id' => 'right-sidebar',
		'description' => 'These widgets will be shown in the Right Sidebar.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
		'before_title' => '<h3>',
		'after_title' => '</h3>'
	) );

	register_sidebar( array(
        'name' => "Header Sidebar",
        'id' => 'header-sidebar',
        'description' => 'These widgets will be shown in the Header Sidebar.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );

    register_sidebar( array(
        'name' => "Footer Sidebar",
        'id' => 'footer-sidebar',
        'description' => 'These widgets will be shown in the Footer Sidebar.',
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h3>',
        'after_title' => '</h3>'
    ) );
}

function comparebet_recent_posts_image_size(){
    add_image_size( 'recent-posts-thumb', 72, 72, true );
}

function comparebet_footer_menus() {
  register_nav_menus(
    array(
      'footer-menu-1' => __( 'Footer menu' ),
      'footer-menu-2' => __( 'Footer main menu' ),
    )
  );
}

function comparebet_fullwidth_latest_image_size(){
    add_image_size( 'fullwidth-latest-post-thumb', 660, 408, true );
}

function comparabet_latest_image_size(){
    add_image_size( 'latest-post-thumb', 210, 132, true );
}

function comparabet_latest_large_image_size(){
    add_image_size( 'latest-large-post-thumb', 720, 360, true );
}

function comparabet_breadcrumbs( $sep = ' > ' ) {
	global $post;
	global $si_ab_ss2_page_data;
	global $wp_query;

	$params                   = si_article_builder_get_custom_subpage_params();
	$event                    = $si_ab_ss2_page_data;
	$pageTournamentTemplateId = isset($event['tournamentTemplateId']) ? $event['tournamentTemplateId'] : "";
	$ttId                     = $pageTournamentTemplateId ? $pageTournamentTemplateId : $params['id'];
	$sb_page_url              = si_article_builder_get_sportsbook_page_link();
	$subPageName              = get_query_var( 'si-sub-page' );
	$label                    = '';
	$url                      = get_site_url();
	$leagueURL = str_replace(' ', '-', strtolower($event['tournamentName']));

	switch ( $subPageName ) {
			case 'league':     //en
			case 'campionato': //it
			case 'campeonato': //es
					$label = !$event["bad"] ? $event['name'] : "--";
					break;
			case 'match':
			case 'compara':
			case 'comparar':
                    $label = '' . '<a href="' . $url . '/' . 'quote-' . $leagueURL . '">' . $event['tournamentName'] . '</a>' . '<span class="separator">' . $sep . '</span>' . $event['homeName'] . ' - ' . $event['awayName'] . '';
					break;
			case 'team':
					$team               = $si_ab_ss2_page_data;
					$mainTournamentId   = $team['mainTournamentId'];
					$mainTournamentName = $team['mainTournament']['name'];

					$label = '' . '<a href="' . $sb_page_url . 'league/' . $mainTournamentId . '/' . '">' . $mainTournamentName . '</a>' . '<span class="separator">' . $sep . '</span>' . $team['name'] . '';
					break;
			case 'player':
					$player = $si_ab_ss2_page_data;
					$teamId = $player['team']['id'];
					$teamName = $player['team']['name'];
					$label  = '' . '<a href="' . $sb_page_url . 'team/' . $teamId . '-' . $teamName . '">' . $teamName . '</a>' . '<span class="separator">' . $sep . '</span>' . $player['name'] . '';
					break;
			default:
					$label = get_the_title();
					break;
	}

	$out = '';
	$out .= '<div>';
	$out .= '<a href="' . home_url( '/' ) . '">Home</a>';
	$out .= '<span class="separator">' . $sep . '</span>';

	if ( is_single() ) {
		$terms = get_the_terms( $post, 'category' );
		if ( is_array( $terms ) && $terms !== array() ) {
			$out .= '<a href="' . get_term_link( $terms[0] ) . '">' . $terms[0]->name . '</a>';
			$out .= '<span class="separator">' . $sep . '</span>';
		}
	}

	if ( is_singular() ) {
		$out .= '<span class="last">' . $label . '</span>';
	}

	if ( is_search() ) {
		$out .= get_search_query();
	}

	if ( is_archive() ) {
        $category = get_queried_object();
        $current_cat_name = $category->name;

        $out .= '<span class="last">' . $current_cat_name . '</span>';
    }

	$out .= '</div>';
	return $out;
}

//Shortcode [fullwidth_latest_post]
function comparebet_fullwidth_latest_post_shortcode_function( $atts ){
    extract( shortcode_atts( array( 'category' => 'Uncategorized' ), $atts ) );

    ob_start();

    $params = array(
        'posts_per_page' => 1,
        'category_name'  => $category,
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'DESC',
    );

    $query = new WP_Query( array_merge($params, array(
        'meta_query' => array(
            array(
                'key'     => 'start_date',
                'value'   => date("Y-m-d H:i:s"),
                'compare' => '>=',
                'type'    => 'DATETIME'
                )
            )
    ) ) );

    if ($query->post_count < 1) {
        $query = new WP_Query( array_merge($params, array( 'meta_query' => array( array( 'key' => 'start_date', 'compare' => 'NOT EXISTS' ) ) ) ) );
    }

    if ( $query->have_posts() ) { ?>
          <div class="fullwidth-latest-post">
            <?php
                $index = 0;
                while ( $query->have_posts() ) : $query->the_post();
                $index++;
                $thumbnail = get_the_post_thumbnail_url( get_the_ID(), 'fullwidth-latest-post-thumb' );
                $post_link = get_page_link();
                $isPostThumbnail = get_the_post_thumbnail();
                $post_image = $isPostThumbnail ? $thumbnail : '';
                $meta = get_post_meta(get_the_ID());
                //home_logo_small
                $home_team_logo_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                $away_team_logo_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';
                $start_date = isset($meta['start_date']) ? $meta['start_date'][0] : get_the_date();
                $postDate = new DateTime($start_date);
                $DD = $postDate->format('j M Y');
            ?>
             <div>
                 <figure>
                        <picture>
                            <div><img src=<?php echo $thumbnail; ?>></div>
                        </picture>
                        <?php if ( !empty($home_team_logo_url) && !empty($away_team_logo_url) )  { ?>
                            <div class="post_teams_logo">
                                <img width="50" height="50" src=<?php echo $home_team_logo_url; ?> alt="Home team logo" />
                                <img width="50" height="50" src=<?php echo $away_team_logo_url; ?> alt="Away team logo" />
                            </div>
                            <?php } ?>
                        <figcaption>
                            <h2><a href=<?php echo $post_link ?>> <?php the_title(); ?> </a></h2>
                            <div>
                                <time><?php echo $DD ?></time>
                                <a class="read-more" href=<?php echo $post_link ?>>Read The Tip <i></i></a>
                            </div>
                        </figcaption>
                 </figure>
             </div>
            <?php endwhile;
            wp_reset_postdata(); ?>
        </div>

        <?php $content = ob_get_clean();
        return $content;
    }
}

//Shortcode [latest_posts]
function comparebet_latest_posts_shortcode_function( $atts ){
        extract( shortcode_atts( array(
            'post_to_show' => '4',
            'category' => 'Uncategorized',
        ), $atts ) );

         ob_start();

         $query = new WP_Query( array(
             'posts_per_page'   => $post_to_show,
             'category_name'    => $category,
             'post_type'        => 'post',
             'orderby'          => 'date',
             'order'            => 'DESC',
             'offset'           => 1,
         ) );

//         $params = array(
//             'category_name'  => $category,
//             'post_type'      => 'post',
//             'orderby'        => 'date',
//             'order'          => 'DESC'
//         );
//
//         $query = new WP_Query( array_merge($params, array(
//             'offset'         => 1,
//             'posts_per_page' => $post_to_show,
//             'meta_query'     => array(
//                 array(
//                     'key'     => 'start_date',
//                     'value'   => date("Y-m-d H:i:s"),
//                     'compare' => '>=',
//                     'type'    => 'DATETIME'
//                     )
//                 ),
//         )));

   if ( $query->have_posts() ) { ?>

         <div class="latest-post-list">
               <?php
                    $index = 0;
                    while ( $query->have_posts() ) : $query->the_post();
                        $index++;
                        $className = '';
                        $large_thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-large-post-thumb' );
                        $thumbnail_src = get_the_post_thumbnail_url( get_the_ID(), 'latest-post-thumb' );
                        $thumbnail = $thumbnail_src;
                        //$post_link = get_page_link();
                        $post_link = get_permalink();
                        $isPostThumbnail = get_the_post_thumbnail();
                        $post_image = $isPostThumbnail ? $large_thumbnail_src : "";
                        $post_image_large = $isPostThumbnail ? $thumbnail : "";
                        $meta = get_post_meta(get_the_ID());
                        $home_team_logo_url = isset($meta['home_logo']) ? $meta['home_logo'][0] : '';
                        $away_team_logo_url = isset($meta['away_logo']) ? $meta['away_logo'][0] : '';
                        $start_date = isset($meta['start_date']) ? $meta['start_date'][0] : get_the_date();
                        $postDate = new DateTime($start_date);
                        $DD = $postDate->format('j. M');
               ?>
                <div class=<?php echo $className?>>
                    <figure>
                        <div><img src=<?php echo $thumbnail; ?>></div>
                            <?php if ( !empty($home_team_logo_url) && !empty($away_team_logo_url) )  { ?>
                                <div class="post_teams_logo">
                                    <img width="50" height="50" src=<?php echo $home_team_logo_url; ?> alt="Home team logo" />
                                    <img width="50" height="50" src=<?php echo $away_team_logo_url; ?> alt="Away team logo" />
                                </div>
                            <?php } ?>
                            <time><?php echo $DD ?></time>
                           <figcaption>
                               <h2><a href=<?php echo $post_link ?>> <?php the_title(); ?> </a></h2>
                           </figcaption>
                    </figure>
                </div>
               <?php endwhile;
               wp_reset_postdata(); ?>
       </div>

       <?php $content = ob_get_clean();
       return $content;
   }
}


// Need for optimization
function comparabet_dns_prefetch_to_preconnect( $urls, $relation_type ) {
	if ( 'dns-prefetch' === $relation_type ) {
		$urls = [];
	}
	if ( 'preconnect' === $relation_type ) {
		$urls = wp_dependencies_unique_hosts();
		//array_push($urls, 'https://www.google-analytics.com');
	}
	return $urls;
}
add_filter( 'wp_resource_hints', 'comparabet_dns_prefetch_to_preconnect', 0, 2 );

function comparabet_disable_emoji_feature() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji');
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji');
	remove_filter( 'embed_head', 'print_emoji_detection_script' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'comparabet_disable_emojis_tinymce' );
	add_filter( 'option_use_smilies', '__return_false' );
}
function comparabet_disable_emojis_tinymce( $plugins ) {
	if( is_array($plugins) ) {
		$plugins = array_diff( $plugins, array( 'wpemoji' ) );
	}
	return $plugins;
}
add_action('init', 'comparabet_disable_emoji_feature');

function comparabet_smartwp_remove_wp_block_library_css() {
    if( is_front_page() || is_home() ){
        wp_dequeue_style( 'wp-block-library' );
        wp_dequeue_style( 'wp-block-library-theme' );
        wp_dequeue_style( 'wc-block-style' );
    }
}
add_action( 'wp_enqueue_scripts', 'comparabet_smartwp_remove_wp_block_library_css', 100 );
add_action( 'enqueue_block_assets', 'comparabet_smartwp_remove_wp_block_library_css', 100 );

function comparabet_remove_gutenberg_styles($translation, $text, $context, $domain) {
	if ( is_front_page() || is_home() ) {
		if($context != 'Google Font Name and Variants' || $text != 'Noto Serif:400,400i,700,700i') {
			return $translation;
		}
	}
	return $translation;
}
add_filter( 'gettext_with_context', 'comparabet_remove_gutenberg_styles',10, 4);

function comparabet_disable_gutenberg() {
  if( is_front_page() || is_home() ) {
    remove_action( 'wp_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
    remove_action( 'admin_enqueue_scripts', 'wp_common_block_scripts_and_styles' );
  }
}
add_action('wp', 'comparabet_disable_gutenberg');

// function comparabet_add_preload_fonts() {
// 	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Montserrat-Regular.woff2' . '" as="font" crossorigin>';
// 	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Montserrat-Bold.woff2' . '" as="font" crossorigin>';
// 	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Roboto-Regular.woff2' . '" as="font" crossorigin>';
// 	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Roboto-Medium.woff2' . '" as="font" crossorigin>';
// 	echo '<link rel="preload" href="' . get_template_directory_uri() . '/fonts/Roboto-Bold.woff2' . '" as="font" crossorigin>';
// }
// add_action( 'wp_head', 'comparabet_add_preload_fonts', 5);

function comparabet_mihdan_add_defer_attribute( $tag, $handle ) {
  $handles = array( 'comparabet-navigation' );

   foreach( $handles as $defer_script) {
      if ( $defer_script === $handle ) {
         return str_replace( ' src', ' defer  src', $tag );
      }
   }

   return $tag;
}
add_filter( 'script_loader_tag', 'comparabet_mihdan_add_defer_attribute', 10, 2 );


//Remove canonical links
remove_action('wp_head', 'rel_canonical');

add_filter ( 'wpseo_canonical' , '__return_false' );