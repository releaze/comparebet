<?php
/*
Template Name: Transparent Page Template
Template Post Type: page
*/

get_header();
?>
<div class="site-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-6">
                    <?php echo comparabet_breadcrumbs( ' > ' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container content">
        <div class="row">
            <div class="col-lg-12 col-xl-9 center-content">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main">
                        <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/content', 'page' );

                                if ( comments_open() || get_comments_number() ) :
                                    comments_template();
                                endif;

                            endwhile; // End of the loop.
                        ?>
                    </main>
                </div>
            </div>
            <div class="col-lg-8 offset-lg-4 col-xl-3 right-sidebar">
                 <aside class="widget-area">
                    <?php dynamic_sidebar( 'right-sidebar' ); ?>
                </aside>
            </div>
        </div>
    </div>
<?php
get_footer();
