<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package comparebet
 */
global $si_ab_ss2_page_data;
$subPageName = get_query_var( 'si-sub-page' );
$event       = $si_ab_ss2_page_data;
$mainSbPageId      = si_article_builder_get_db_value( "mainPageId" );
$isLeaguePageLabel = ["league", "campionato", "campeonato"];
$isMatchPageLabel = ["match", "compara", "comparar"];
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
        <?php
            if (in_array($subPageName, $isLeaguePageLabel)) {
                ?>
                    <h1 class="entry-title"><?php echo $event['name']; ?></h1>
                <?php
            }

            if (in_array($subPageName, $isMatchPageLabel)) {
                ?>
                    <h1 class="entry-title"><?php echo get_the_title(); ?></h1>
                <?php
            }
        ?>
	</header><!-- .entry-header -->

	<?php comparebet_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'comparebet' ),
				'after'  => '</div>',
			)
		);
		?>
	</div><!-- .entry-content -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
			edit_post_link(
				sprintf(
					wp_kses(
						/* translators: %s: Name of current post. Only visible to screen readers */
						__( 'Edit <span class="screen-reader-text">%s</span>', 'comparebet' ),
						array(
							'span' => array(
								'class' => array(),
							),
						)
					),
					wp_kses_post( get_the_title() )
				),
				'<span class="edit-link">',
				'</span>'
			);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-<?php the_ID(); ?> -->
