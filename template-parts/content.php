<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package comparebet
 */
include_once ABSPATH . 'wp-admin/includes/plugin.php';

$post_id = get_the_ID();
$team_1  = get_post_meta($post_id, 'team_1', true);
$team_2  = get_post_meta($post_id, 'team_2', true);

$show_default_header  = true;
$show_default_content = true;

?><article id="post-<?php the_ID(); ?>" <?php post_class(); ?>><?php

if ($team_1 && $team_2 && (is_plugin_active('si-article-builder/si-article-builder.php') || is_plugin_active('si-article-builder-dev/si-article-builder.php')) && ini_get( 'allow_url_fopen' )) {
	$eventJson = file_get_contents( sprintf( 'https://ss2.tjekscores.dk/events-v2?sportId=1&teamId1=%s&teamId2=%s&fromDate=%s&limit=1', $team_1, $team_2, date('Y-m-d', strtotime("-2 day")) ), true );

	if ( ! is_wp_error( $eventJson ) ) {
		$events = json_decode( $eventJson );
		$event  = isset( $events->events ) && is_array( $events->events )&& isset( $events->events[0] ) ? $events->events[0] : false;

		if ($event && $event->eventId) {
			$event_id    = $event->eventId;
			$start_date  = $event->startDate;
			$status_type = $event->statusType;
			$date1       = date_create('NOW');
			$date2       = date_create($start_date);
			$diff        = date_diff($date1, $date2);
			$diff_days   = $diff->d;

			$si_ab_ss2_page_data    = json_decode(json_encode($event), true);
			$si_ab_params_from_post = array('event_id' => $event_id);

			$tpl_path = WP_PLUGIN_DIR . '/si-article-builder/template-parts/content/sportsbook/content-match-sub.php';

			if (!is_file($tpl_path)) {
				$tpl_path = WP_PLUGIN_DIR . '/si-article-builder-dev/template-parts/content/sportsbook/content-match-sub.php';
			}

			$show_default_header  = false;
			$show_default_content = false;

      if ($status_type === "notstarted" && $diff->d <= 2) {
				require $tpl_path;
      } elseif ($status_type === "notstarted" && $diff->d <= 60) {
				$show_default_content = true;
				echo '<div class="entry-group" style="margin-bottom: 20px;">';
				printf('<si-lb-widget widget-id="MatchInfoWithOdds" width="100%%" nopadding="true" transparent="true" card="false" event-id="%s"></si-lb-widget>', $event_id);
				echo '</div>';
      } elseif ($status_type === "inprogress") {
				require $tpl_path;
			} elseif ($status_type === "finished" && $diff->d < 1 && $diff->d >= -2) {
				require $tpl_path;
			}
		}
	}
}

if ($show_default_header) { ?>
	<div class="entry-group">
        <figure>
            <?php comparebet_post_thumbnail(); ?>

            <figcapture>
                <header class="entry-header">
                    <?php
                    if ( is_singular() ) :
                            the_title( '<h1 class="entry-title">', '</h1>' );
                    else :
                            the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
                    endif;

                    if ( 'post' === get_post_type() ) :
                            ?>
                            <!-- <div class="entry-meta">
                                    <?php
                                    comparebet_posted_on();
                                    comparebet_posted_by();
                                    ?>
                            </div> -->
                    <?php endif; ?>
                </header>
            </figcapture>
        </figure>

        <header class="entry-header mobile">
            <?php
            if ( is_singular() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
            else :
                    the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
            endif;

            if ( 'post' === get_post_type() ) :
                    ?>
                    <!-- <div class="entry-meta">
                            <?php
                            comparebet_posted_on();
                            comparebet_posted_by();
                            ?>
                    </div> -->
            <?php endif; ?>
        </header>
	</div>
<?php }
if ($show_default_content) { ?>
	<div class="entry-content">
		<div class="parent-selector"></div>
		<?php
		the_content(
			sprintf(
				wp_kses(
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'comparebet' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'comparebet' ),
				'after'  => '</div>',
			)
		);
		?>
</div>
<?php } ?>
</article><!-- #post-<?php the_ID(); ?> -->
