<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package comparebet
 */

get_header();
?>
    <div class="site-breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-xl-6">
                    <?php echo comparabet_breadcrumbs( ' > ' ); ?>
                </div>
            </div>
        </div>
    </div>

    <div class="container content">
        <div class="row">
            <div class="col-lg-12 col-xl-9 center-content">
                <div id="primary" class="content-area">
                    <main id="primary" class="site-main">
                        <div class="si-default-post-layout">
                                <?php if ( have_posts() ) : ?>
                                <?php
                                    $category = get_queried_object();
                                    $current_cat_id = $category->term_id;
                                    $current_cat_name = str_replace(' ', '-', strtolower($category->name));
                                ?>
                                    <header class="page-header">
                                        <h1 class="page-title">
                                            <?php echo $category->name; ?>
                                        </h1>
                                    </header><!-- .page-header -->

                                    <!-- Category description -->
                                    <?php
                                        the_archive_description( '<div class="taxonomy-description">', '</div>' );
                                    ?>


                                    <!-- Need to check meta_query -->
                                    <!-- <?php echo do_shortcode( '[fullwidth_latest_post category=' . $current_cat_name . ']' ); ?> -->

                                    <?php echo do_shortcode( '[latest_posts category=' . $current_cat_name . ' post_to_show="6"]' ); ?>
                                    <si-lb-widget widget-id="HotbetFinder" provider="comeon" nopadding="true" width="100%"></si-lb-widget>

                                    <?php
                                    /* Start the Loop */
                                   /*  while ( have_posts() ) :
                                        the_post();

                                         *//*
                                         * Include the Post-Type-specific template for the content.
                                         * If you want to override this in a child theme, then include a file
                                         * called content-___.php (where ___ is the Post Type name) and that will be used instead.
                                         *//*
                                        get_template_part( 'template-parts/content', get_post_type() );

                                    endwhile; */

                                    //the_posts_navigation();

                                else :

                                    get_template_part( 'template-parts/content', 'none' );

                                endif;

                            ?>
                        </div>
                    </main><!-- #main -->
                </div>
            </div>
            <div class="col-lg-8 offset-lg-4 col-xl-3 right-sidebar">
                 <aside class="widget-area">
                    <?php dynamic_sidebar( 'right-sidebar' ); ?>
                </aside>
            </div>
        </div>
    </div>
<?php
get_footer();
