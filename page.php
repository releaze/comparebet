<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by defaulease note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package comparebet
 */

get_header();
?>
    <div class="container content">
            <div class="row">
                <div class="col-lg-12 col-xl-9 center-content bg-content">
                    <div id="primary">
                        <main id="main" class="site-main">
                            <?php
                                while ( have_posts() ) :
                                    the_post();

                                    get_template_part( 'template-parts/home', 'page' );

                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;

                                endwhile; // End of the loop.
                            ?>

                        </main>
                    </div>
                </div>
                <div class="col-lg-8 offset-lg-4 col-xl-3 right-sidebar">
                     <aside class="widget-area">
                         <?php dynamic_sidebar( 'right-sidebar' ); ?>
                     </aside>
                </div>
            </div>
        </div>
    <?php
    get_footer();
