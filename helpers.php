<?php
/**
 * Get page url
 *
 * @return string
 */
function comparabet_get_page_url() {
    global $post;
    $post_slug = $post->post_name;
    $pageLink = get_query_var( 'si-sub-page' ) . '/' . get_query_var( 'si-sub-sub-page' );

    return get_query_var( 'si-sub-page' ) ? 'sportsbook/' . $pageLink : $post_slug;
}
