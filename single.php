<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package comparebet
 */

get_header();
?>
<div class="site-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-xl-6">
                <?php echo comparabet_breadcrumbs( ' > ' ); ?>
            </div>
        </div>
    </div>
</div>

<div class="container content">
    <div class="row">
        <div class="col-lg-12 col-xl-9 center-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="si-default-post-layout">
                        <?php
                            while ( have_posts() ) :
                                the_post();

                                get_template_part( 'template-parts/content', get_post_type() );

                                ?>

                                <?php

                                the_post_navigation(
                                    array(
                                        //'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Vecchi post:', 'comparebet' ) . '</span> <span class="nav-title">%title</span>',
                                        //'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Nuovi post:', 'comparebet' ) . '</span> <span class="nav-title">%title</span>',
                                        'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Vecchi post', 'comparebet' ) . '</span>',
                                        'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Nuovi post', 'comparebet' ) . '</span>',
                                    )
                                );

                            endwhile; // End of the loop.
                            ?>
                    </div>
                </main>
            </div>
        </div>

        <div class="col-lg-8 offset-lg-4 col-xl-3 right-sidebar">
           <aside class="widget-area">
               <?php dynamic_sidebar( 'right-sidebar' ); ?>
           </aside>
        </div>
    </div>
</div>
<?php
get_footer();
