<?php
    // register SI_Recent_Posts_Widget
    function register_si_recent_posts_widget() {
        register_widget( 'SI_Recent_Posts_Widget' );
    }

    add_action( 'widgets_init', 'register_si_recent_posts_widget' );

    function register_dt9_leagues_menu_widget() {
        register_widget( 'DT9_Leagues_Menu_Widget' );
    }
    
    add_action( 'widgets_init', 'register_dt9_leagues_menu_widget' );
    
?>
