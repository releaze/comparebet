<?php
/**
 * Adds DT9_Leagues_Menu_Widget  widget.
 */
// global $si_ab_ss2_page_data;

class DT9_Leagues_Menu_Widget extends WP_Widget {
	public $preparedTournaments = array();

	public $topTournaments = [
		42   => "Champions League",
		46   => "3F Superleague",
		47   => "Premier League",
		48   => "England Championship",
		50   => "European Championship",
		53   => "Ligue 1",
		54   => "1 Bundesliga",
		55   => "Serie A",
		57   => "Eredivisie",
		59   => "Eliteserien",
		61   => "Primeira Liga",
		67   => "Allsvenskan",
		71   => "Super Lig",
		73   => "Europa League",
		76   => "World Cup",
		77   => "Coppa del mondo",
		85   => "Division 1",
		87   => "La Liga",
		108  => "League One",
		109  => "League Two",
		132  => "FA Cup",
		133  => "EFL Cup",
		138  => "Copa del Rey",
		141  => "Coppa Italia",
		146  => "Bundesliga",
		239  => "Division 2",
		245  => "Superligaen Qualification",
		9375 => "Champions League",
		9806 => "UEFA Nations League A",
		9807 => "UEFA Nations League B",
		9808 => "UEFA Nations League C",
		9809 => "UEFA Nations League D",
	];

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'dt9_leagues_menu_widget', // Base ID
			esc_html__( 'DT9 Leagues Menu', 'text_domain' ), // Name
			array( 'description' => esc_html__( 'Top leagues menu', 'text_domain' ), ) // Args
		);
	}

	/**
	 * Global function to get preparedTournaments
	 *
	 * @return array
	 */
	public function getPreparedTournaments() {
		$supportedTournaments = si_article_builder_get_sb_leagues_list();


		// Filter by clients leagues
		foreach ($supportedTournaments as $id) {
			if (in_array($id, $supportedTournaments)) {
				$this->preparedTournaments[$id]=$this->topTournaments[$id];
			}
		}

		$this->preparedTournaments = empty($this->preparedTournaments) ? $this->topTournaments : $this->preparedTournaments;

		return $this->preparedTournaments;
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		echo $args['before_widget'];
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
		}

		global $si_ab_ss2_page_data;
		$params                   = si_article_builder_get_custom_subpage_reverse_params();
		$event                    = $si_ab_ss2_page_data;
		// $pageTournamentTemplateId for make active item
		// $pageTournamentTemplateId =  $event['tournamentTemplateId'];
		$ttId                     = isset($event['tournamentTemplateId']) && $event['tournamentTemplateId'] ? $event['tournamentTemplateId'] : $params['id'];
		$sbRoutersSlugs           = array_flip( json_decode( si_article_builder_get_db_value( 'sb_routers_slugs' ), true ) );

		$mainPageId               = si_article_builder_get_db_value( 'mainPageId' );
		$sb_page_url              = si_article_builder_get_sportsbook_page_link();
		$sb_page_url              = $sb_page_url . (isset($sbRoutersSlugs) && isset($sbRoutersSlugs['league']) ? $sbRoutersSlugs['league'] : 'league');

		$nationalIds              = array(42,50,73,76,77,9375,9806,9807,9808,9809);
		$leagues                  = array();
		$nationalLeagues          = array();
		$tournaments              = $this->getPreparedTournaments();

		$language                 = si_article_builder_get_db_value( 'widgets_language' );
		//$leaguesTitle             = $language == "en" ? "Leagues" : "Tornei";
		//$nationalLeaguesTitle     = $language == "en" ? "International Competitions" : "Competizioni Internazionali";
		$leaguesTitle             = ! empty( $instance['league-title'] ) ? $instance['league-title'] : esc_html__( 'Leagues', 'text_domain' );
		$nationalLeaguesTitle     = ! empty( $instance['national-league-title'] ) ? $instance['national-league-title'] : esc_html__( 'International Competitions', 'text_domain' );
		$URLPrefix                = ! empty( $instance['league-url-prefix'] ) ? $instance['league-url-prefix'] : esc_html__( 'quote', 'text_domain' );
		$url                      = get_site_url();

		// $topTournaments
		// Filter by sample league | national league
		foreach ($tournaments as $id => $name) {
			if ($this->topTournaments[$id]) {
					$leagues[$id]=$this->topTournaments[$id];
			}
		}

		?>
		    <div id="dt9-leagues-menu-widget">

		    	<!-- List1 -->
				<div class="dt9-leagues" style="width:<?php echo (count($leagues)*170) + 44; ?>px">
					<div class="dt9-league-left-button dt9-league-button"><div><</div></div>
						<?php
							$index = 0;
							foreach ($leagues as $id => $name) {
								$isSameId = $id == $ttId;
								$className = $isSameId ? 'active' : '';
								$index++;
								$isHidden = $index > 7;
								$hiddenClassName = $isHidden ? 'hidden' : '';
								?>
									<div class="dt9-league-card" class=<?php echo "$hiddenClassName" ?>>
										<a href=<?php echo $url  . "/" . $URLPrefix . "-" . str_replace(' ', '-', strtolower($name)) ?> class=<?php echo "$className" ?>>
											<picture>
												<source srcset=<?php echo "https://d2ylwairq77d9m.cloudfront.net/leagues/64x64/webp/$id.webp" ?> type="image/webp" />
												<img class="si-league-logo" alt=<?php echo "$id" ?> src="https://d2ylwairq77d9m.cloudfront.net/leagues/64x64/$id.png">
											</picture>
											<span><?php echo "$name"; ?></span>
										</a>
									</div>
								<?php
							}
						?>
						<div class="dt9-league-right-button dt9-league-button"><div>></div></div>
				</div>
            </div>
		<?php

		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$leagueTitle = ! empty( $instance['league-title'] ) ? $instance['league-title'] : esc_html__( 'Leagues', 'text_domain' );
		$nationalLeagueTitle = ! empty( $instance['national-league-title'] ) ? $instance['national-league-title'] : esc_html__( 'International Competitions', 'text_domain' );
		$URLPrefix = ! empty( $instance['league-url-prefix'] ) ? $instance['league-url-prefix'] : esc_html__( 'quote', 'text_domain' );
		?>
		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'league-title' ) ); ?>"><?php esc_attr_e( 'Leagues title:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'league-title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'league-title' ) ); ?>" type="text" value="<?php echo esc_attr( $leagueTitle ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'national-league-title' ) ); ?>"><?php esc_attr_e( 'National leagues title:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'national-league-title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'national-league-title' ) ); ?>" type="text" value="<?php echo esc_attr( $nationalLeagueTitle ); ?>">
		</p>

		<p>
			<label for="<?php echo esc_attr( $this->get_field_id( 'url-prefix' ) ); ?>"><?php esc_attr_e( 'URL prefix:', 'text_domain' ); ?></label>
			<input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'league-url-prefix' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'league-url-prefix' ) ); ?>" type="text" value="<?php echo esc_attr( $URLPrefix ); ?>">
		</p>
		<?php
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['league-title'] = ( ! empty( $new_instance['league-title'] ) ) ? sanitize_text_field( $new_instance['league-title'] ) : '';
		$instance['national-league-title'] = ( ! empty( $new_instance['national-league-title'] ) ) ? sanitize_text_field( $new_instance['national-league-title'] ) : '';
		$instance['league-url-prefix'] = ( ! empty( $new_instance['league-url-prefix'] ) ) ? sanitize_text_field( $new_instance['league-url-prefix'] ) : '';

		return $instance;
	}

} // class DT9_Leagues_Menu_Widget

