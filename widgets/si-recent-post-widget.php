<?php
/**
 * Adds SI_Recent_Posts_Widget widget.
 */
class SI_Recent_Posts_Widget extends WP_Widget {

    /**
     * Register widget with WordPress.
     */
    function __construct() {
        parent::__construct(
            'si_recent_posts_widget', // Base ID
            esc_html__( 'SI Recent Posts', 'text_domain' ), // Name
            array( 'description' => esc_html__( 'Sports Innovation Recent Posts Widget', 'text_domain' ), ) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args     Widget arguments.
     * @param array $instance Saved values from database.
     */
        public function widget( $args, $instance ) {
        $number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
        if ( ! $number ) {
            $number = 5;
        }

        /**
         * Filters the arguments for the Recent Posts widget.
         *
         * @since 3.4.0
         * @since 4.9.0 Added the `$instance` parameter.
         *
         * @see WP_Query::get_posts()
         *
         * @param array $args     An array of arguments used to retrieve the recent posts.
         * @param array $instance Array of settings for the current widget.
         */
        $r = new WP_Query(
            apply_filters(
                'widget_posts_args',
                array(
                    'posts_per_page'      => $number,
                    'no_found_rows'       => true,
                    'post_status'         => 'publish',
                    'ignore_sticky_posts' => true,
                    'term_id' => "-15",
                ),
                $instance
            )
        );

        if ( ! $r->have_posts() ) {
            return;
        }

        echo $args['before_widget'];
        if ( ! empty( $instance['title'] ) ) {
            echo $args['before_title'] . apply_filters( 'widget_title', $instance['title'] ) . $args['after_title'];
        }

        ?>
            <ul class="si-recent-posts-widget">
                <?php foreach ( $r->posts as $recent_post ) : ?>
                    <?php
                        $post_title    = get_the_title( $recent_post->ID );
                        $title         = ( ! empty( $post_title ) ) ? $post_title : __( '(no title)' );
                        $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( $recent_post->ID ), 'recent-posts-thumb' );
                        $aria_current  = '';
                        //$post_image    = $thumbnail_src ? $thumbnail_src[0] : "http://comparebet.dev.si-ab.com/wp-content/uploads/sites/11/2020/12/default-72x72.jpg";
                        $post_image    = $thumbnail_src ? $thumbnail_src[0] : "";

                        if ( get_queried_object_id() === $recent_post->ID ) {
                            $aria_current = ' aria-current="page"';
                        }
                    ?>
                    <li>
                        <figure>
                            <div><img src="<?php echo $post_image; ?>" alt="" onerror="this.style.visibility='hidden'" alt=""></div>
                            <figcaption>
                                <a href="<?php the_permalink( $recent_post->ID ); ?>" class="title">
                                    <?php echo $title; ?>
                                </a>
                            </figcaption>
                        </figure>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php
            echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form( $instance ) {
        $title     = ! empty( $instance['title'] ) ? $instance['title'] : esc_html__( 'New title', 'text_domain' );
        $number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;

        ?>
        <p>
            <label for="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>"><?php esc_attr_e( 'Title:', 'text_domain' ); ?></label>
            <input class="widefat" id="<?php echo esc_attr( $this->get_field_id( 'title' ) ); ?>" name="<?php echo esc_attr( $this->get_field_name( 'title' ) ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:' ); ?></label>
            <input class="tiny-text" id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="number" step="1" min="1" value="<?php echo $number; ?>" size="3" />
        </p>

        <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update( $new_instance, $old_instance ) {
        $instance              = $old_instance;
        $instance['title']     = sanitize_text_field( $new_instance['title'] );
        $instance['number']    = (int) $new_instance['number'];
        return $instance;
    }

} // class SI_Recent_Posts_Widget
