<?php
 /*
Template Name: Left sidebar template
Template Post Type: post
*/

get_header();
?>
<div class="site-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3"></div>
            <div class="col-lg-8 col-xl-6">
                <?php echo comparabet_breadcrumbs( ' > ' ); ?>
            </div>
        </div>
    </div>
</div>

<div class="container content">
    <div class="row">
        <div class="col-lg-4 col-xl-3 left-sidebar">
           <aside class="widget-area">
               <?php get_sidebar(); ?>
           </aside>
        </div>
        <div class="col-md-9 center-content">
            <div id="primary" class="content-area">
                <main id="main" class="site-main">
                    <div class="si-left-post-layout">
                        <?php
                                while ( have_posts() ) :
                                    the_post();

                                    get_template_part( 'template-parts/content', get_post_type() );

                                    the_post_navigation(
                                        array(
                                            'prev_text' => '<span class="nav-subtitle">' . esc_html__( 'Previous:', 'comparebet' ) . '</span> <span class="nav-title">%title</span>',
                                            'next_text' => '<span class="nav-subtitle">' . esc_html__( 'Next:', 'comparebet' ) . '</span> <span class="nav-title">%title</span>',
                                        )
                                    );

                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) :
                                        comments_template();
                                    endif;

                                endwhile; // End of the loop.
                                ?>
                    </div>
                </main>
            </div>
        </div>
    </div>
</div>
<?php
get_footer();
