if(document.querySelector("#dt9-leagues-menu-widget")){
    const dt9LeaguesMenu = document.querySelector("#dt9-leagues-menu-widget");
    const menu = document.querySelector(".site-branding-wrapper");
    const buttons = document.querySelectorAll(".dt9-league-button");
    document.addEventListener('scroll', function(e){ 

        if(document.documentElement.scrollTop > 40){

            dt9LeaguesMenu.style.position = "fixed";
            dt9LeaguesMenu.style.top = '70px';
            dt9LeaguesMenu.style.left = 0;
            dt9LeaguesMenu.style.right = 0;
            dt9LeaguesMenu.style.zIndex = 9998;

            menu.style.position = "fixed";
            menu.style.top = 0;
            menu.style.left = 0;
            menu.style.right = 0;
            menu.style.zIndex = 9999;

            for (let index = 0; index < buttons.length; index++) {
                const element = buttons[index];
                element.style.top = '69px';
            }
        }else{
            dt9LeaguesMenu.style.position = "relative";
            dt9LeaguesMenu.style.top = 0;
            menu.style.position = "relative";
            menu.style.zIndex = 9999;

            for (let index = 0; index < buttons.length; index++) {
                const element = buttons[index];
                element.style.top = '69px';
            }
        }

    }, true);

    document.querySelector(".dt9-league-left-button").onclick = function(){
        dt9LeaguesMenu.scroll({
            left:  dt9LeaguesMenu.scrollLeft - 170,
            behavior: 'smooth' 
          });
    }

    document.querySelector(".dt9-league-right-button").onclick = function(){
        dt9LeaguesMenu.scroll({
            left:  dt9LeaguesMenu.scrollLeft + 170,
            behavior: 'smooth' 
          });
    }
}

