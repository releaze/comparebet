<?php
 /*
Template Name: Landing template
Template Post Type: post
*/

get_header('landing');

$query = array(
	'mainProvider'     => $_GET['mainProvider'],
	'betKey'           => $_GET['betKey'],
	'eventId'          => $_GET['eventId'],
	'providers'        => $_GET['providers'],
	'primaryBookmaker' => $_GET['primaryBookmaker'],
	'acid'             => $_GET['acid'],
);

$eventId = $query['eventId'];
$mainProvider = $query['mainProvider'];
$betKey = $query['betKey'];
$providers = $query['providers'];
$primaryBookmaker = $query['primaryBookmaker'];
$acid = $query['acid'];

$language = si_article_builder_get_db_value( 'widgets_language' );

$i18 = [
    'en' => [
        'form'              => 'Form',
        'head_to_head'      => 'Head to Head',
        'fun_fact'          => 'Fun fact',
        'important_players' => 'Important players',
        'last_10_matches'   => 'Last 10 matches',
        'goals'             => 'Goals',
    ],
    'it' => [
        'form'              => 'Modulo',
        'head_to_head'      => 'Testa a testa',
        'fun_fact'          => 'Fatto divertente',
        'important_players' => 'Giocatori importanti',
        'last_10_matches'   => 'Ultime 10 partite',
        'goals'             => 'Obiettivi',
    ],
];

?>
<div class="landing-subpage-content">
    <div class="bg">
        <?php if ( ($eventId && $betKey && $eventId != "undefined" && $betKey != "undefined") ) { ?>
            <section class="bg-container">
                <figure><img src="https://comparabet.it/wp-content/uploads/2021/02/comparabet.png" /></figure>

                <div class="widget">
                    <si-lb-widget widget-id="BettingPopupBanner" width="100%" transparent="true" card="true" event-id="<?php echo $eventId; ?>" bet-key="<?php echo $betKey; ?>" data-providers="<?php echo $providers; ?>" data-acid="<?php echo $acid; ?>" data-primary-bookmaker="<?php echo $primaryBookmaker; ?>" data-locale="<?php echo $language; ?>" data-main-button-text="Verifica la migliore quota"></si-lb-widget>
                </div>
            </section>
        <?php } ?>


        <?php if ( !$eventId || !$betKey || $eventId == "undefined" || $betKey == "undefined" ) { ?>
            <div class="error-message">You have not transferred enough data!</div>
        <?php } ?>
    </div>

    <?php if ( ($eventId && $betKey && $eventId != "undefined" && $betKey != "undefined") ) { ?>
        <section class="widget-container">
            <div>
                <h2><?php echo $i18[$language]['form']; ?></h2>
                <si-lb-widget widget-id="MTSLatestMatches" across-all-tournaments="true" limit="5" width="100%" event-id="<?php echo $eventId; ?>" transparent="true" data-locale="<?php echo $language; ?>"></si-lb-widget>
            </div>
            <div>
                <h2><?php echo $i18[$language]['head_to_head']; ?></h2>
                <si-lb-widget widget-id="MTSH2HStats" width="100%" event-id="<?php echo $eventId; ?>" transparent="true" data-locale="<?php echo $language; ?>"></si-lb-widget>
            </div>
            <div>
                <h2><?php echo $i18[$language]['fun_fact']; ?></h2>
                <si-lb-widget widget-id="EventTeamFunStats" nopadding="true" fault="2" limit="10" width="100%" event-id="<?php echo $eventId; ?>" transparent="true" data-locale="<?php echo $language; ?>"></si-lb-widget>
            </div>

            <!-- <div>
                <h2><?php echo $i18[$language]['important_players']; ?></h2>
                <div>this widget requires tournamentId and seasonId</div>
            </div> -->
            <div>
                <h2><?php echo $i18[$language]['last_10_matches']; ?></h2>
                <si-lb-widget widget-id="EventTeamsLatestStatsCompare" width="100%" event-id="<?php echo $eventId; ?>" transparent="true" data-locale="<?php echo $language; ?>"></si-lb-widget>
            </div>
            <div>
                <h2><?php echo $i18[$language]['goals']; ?></h2>
                <si-lb-widget widget-id="EventTeamsOUStatsCompare" width="100%" event-id="<?php echo $eventId; ?>" transparent="true" hide-controls="false" data-locale="<?php echo $language; ?>"></si-lb-widget>
            </div>
        </section>

        <section class="widget-container footer">
            <div>
                <si-lb-widget widget-id="BettingPopupBanner" transparent="true" width="900" event-id="<?php echo $eventId; ?>" horizontal="true" horizontal-view="true" card="true" bet-key="<?php echo $betKey; ?>" data-providers="<?php echo $providers; ?>" data-acid="<?php echo $acid; ?>" data-locale="<?php echo $language; ?>" data-primary-bookmaker="<?php echo $primaryBookmaker; ?>"></si-lb-widget>
            </div>
        </section>
    <?php } ?>
</div>
<div class='empty-footer'>
    <?php
    get_footer();
    ?>
</div>
