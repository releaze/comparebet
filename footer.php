<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package comparebet
 */

?>
	<footer id="colophon" class="site-footer">
	    <div class="site-info">
            <div class="container">
                <div class="row">
                    <div class="col col-lg-3">
                        <div class="column">
                            <h3>Comparabet</h3>
                            <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'footer-menu-2',
                                    'menu_class'     => 'menu',
                                 ));
                             ?>
                        </div>
                    </div>
                    <div class="col col-lg-3">
                        <div class="column">
                            <h3>Company</h3>
                            <?php wp_nav_menu(
                                array(
                                    'theme_location' => 'footer-menu-1',
                                    'menu_class' => 'menu',
                                 ));
                             ?>
                         </div>
                    </div>
                    <div class="col-lg-6 right-align">
                        <div class="column">
                            <?php dynamic_sidebar( 'footer-sidebar' ); ?>
                        </div>
                    </div>
                </div>

                <div class="privacy-info">
                    <div>Il gioco è vietato ai minori e può causare dipendenza patologica. Per info, regolamenti, bonus e probabilità di vincita: <a href="https://www.adm.gov.it">www.adm.gov.it</a></div>
                    <div>Comparabet è un portale informativo che offre servizi di comparazione quote ai sensi della L. 96/2018 e delle Linee Guida Agcom (delibera n. 132/19/CONS del 18 aprile 2019)</div>
                </div>
            </div>
	    </div>
	</footer>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
