<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package comparebet
 */

$post_id   = get_the_ID();
$team_1    = get_post_meta($post_id, 'team_1', true);
$team_2    = get_post_meta($post_id, 'team_2', true);

$newParams = si_article_builder_get_custom_subpage_reverse_params();
$params    = si_article_builder_get_custom_subpage_params();
$eventId   = isset($newParams['id']) && is_numeric($newParams['id']) ? $newParams['id'] : $params['id'];

$eventJson = file_get_contents(sprintf('https://ss2.tjekscores.dk/events-v2?eventId=%s', $eventId), true);
$events    = json_decode($eventJson);
$event     = isset($events->events) && is_array($events->events) && isset($events->events[0]) ? $events->events[0] : false;

$homeID    = empty($team_1) ? $event->homeId : $team_1;
$awayID    = empty($team_2) ? $event->awayId : $team_2;

//$homeID    = $event->homeId;
//$awayID    = $event->awayId;

$evergreen_post = get_posts(
    array(
        'posts_per_page' => -1,
        // 'tag'            => 'ab-evergreen-teams',
        'post_status'    => array('publish'),
        'post_type'      => 'post',
        'orderby'        => 'date',
        'order'          => 'DESC',
        'meta_query' => array(
            array(
                'key'     => 'team_1',
                'value'   => array($homeID, $awayID),
                'compare' => 'IN',
            ),
            array(
                'key'     => 'team_2',
                'value'   => array($homeID, $awayID),
                'compare' => 'IN',
            )
        )
    )
);
$ev_post    = $evergreen_post[0];
$post_name  = $ev_post->post_name;
$url        = get_site_url();

$language = 'italian';
$lang_class = 'it';
$lang_code = 'it-IT';


switch (get_current_blog_id()) {
    case 1: //it default
    default:
        break;
    case 4: //es
        $language = "español";
        $lang_class = "es";
        $lang_code = 'es-ES';
        break;
}
?>
<!doctype html>
<html lang="<?php echo $lang_code; ?>" prefix="og: https://ogp.me/ns#">

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php wp_head(); ?>
    <?php
    /*if (!empty($team_1) || !empty($team_2) || $evergreen_post) {
        $ev_post    = $evergreen_post[0];
        $post_name  = $ev_post->post_name;
        $url        = get_site_url();
        //rel_canonical();
    ?>
        <!-- Canonical URL by Evergreen Post -->
        <link rel="canonical" href=<?php echo $url  . "/" . $post_name ?>>
    <?php
    }*/
    ?>

    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-6SJE5FPVSH"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'G-6SJE5FPVSH');
    </script>
</head>

<body <?php body_class($lang_class); ?>>
    <?php wp_body_open(); ?>
    <div id="page" class="site">
        <a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e('Skip to content', 'comparebet'); ?></a>

        <header id="masthead" class="site-header">
            <div class="site-branding-wrapper">
                <div style="display:flex;">
                    <div class="site-branding" style="width:270px;">
                        <?php
                        the_custom_logo();
                        ?>
                    </div><!-- .site-branding -->
                    <div class="header-sidebar">
                        <nav id="site-navigation" class="main-navigation">
                            <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
                                <span><em></em></span>
                            </button>

                           
                            <div id="general-menu">
                                <div class="mobile-tabs">
                                    <ul>
                                        <li class="active">
                                            <?php
                                            $icon = get_stylesheet_directory_uri() . '/assets/icons/menu-icon.svg';
                                            echo file_get_contents($icon);
                                            ?>
                                            Menu
                                        </li>
                                        <li>
                                            <?php
                                            $icon = get_stylesheet_directory_uri() . '/assets/icons/trophy-icon.svg';
                                            echo file_get_contents($icon);
                                            ?>
                                            Leagues
                                        </li>
                                    </ul>
                                </div>

                                <div id="menus">
                                    <?php
                                    wp_nav_menu(
                                        array(
                                            'theme_location' => 'menu-1',
                                            'menu_id'        => 'primary-menu',
                                            'menu_class'     => 'menu show',
                                        )
                                    );
                                    ?>

                                    <?php
                                    if (class_exists('Leagues_Menu_Widget')) {
                                        $my_class = new Leagues_Menu_Widget;
                                        $tournaments = $my_class->getPreparedTournaments();
                                    }
                                    ?>
                                    <ul class="mobile-leagues-menu leagues">
                                        <?php
                                        $params = si_article_builder_get_custom_subpage_params();
                                        $ttId = $params['id'];
                                        $url = get_site_url();

                                        if (!$tournaments) {
                                        ?>
                                            <li>Do not have items</li>
                                        <?php
                                        }

                                        foreach ($tournaments as $id => $name) {
                                            $isSameId = $id == $ttId;
                                            $className = $isSameId ? 'active' : '';
                                        ?>
                                            <li class=<?php echo "$className" ?>>
                                                <a href=<?php echo $url  . "/" . "quote-" . str_replace(' ', '-', strtolower($name)) ?> class=<?php echo "$className" ?>>
                                                    <?php echo "$name"; ?>
                                                </a>
                                            </li>
                                        <?php
                                        }
                                        ?>
                                    </ul>

                                </div>

                                <script>
                                    const links = document.getElementById("menus");
                                    const toggleMenu = document.querySelector('.mobile-tabs ul');
                                    const items = toggleMenu.getElementsByTagName("li");

                                    for (let i = 0; i < items.length; i++) {
                                        items[i].addEventListener("click", function(event) {
                                            if (event.target.tagName === 'LI') {
                                                const current = document.querySelector(".active");
                                                current.classList.remove('active');
                                                this.classList.add('active');

                                                const activeMenuName = items[i].innerText.toLowerCase();
                                                const prevLinksMenu = document.querySelector(".show");
                                                prevLinksMenu.classList.remove('show');

                                                const activeLinksMenu = document.querySelector(`.${activeMenuName}`);
                                                activeLinksMenu.classList.add('show');
                                            }
                                        });
                                    }
                                </script>
                            </div>
                        </nav><!-- #site-navigation -->

                    </div>
                </div>
            </div>

            <div class="bg"></div>
        </header><!-- #masthead -->
        <div>

            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('secondary_header_menu')) :

            endif; ?>

        </div>